# bPOL12V

The board purpose is test the bPOL12V behaviour in a single pcb. If the first test go successful, it will be mounted afterwards in the system board for more testing. The board was designed mechanically compatible to replace the 2 pcbs base in FEASTMP.

|![](images/bPOL12V.jpg)|
|:--:|
| *DIOT bPOL12V test board* |


In the image below is shown how the bPOL board is connected with the DIOT system board. The bPOL board generates 3.3V and 1.2V like the FEASTMP original scheme, but in addition it also generates 2.5V. A wire is needed to shortcut the LT3083, which provided originally 2.5V.
|![](images/system_board_bPOL12V.jpg)|
|:--:|
| *DIOT system board with bPOL12V* |

## Maintainers
- [Greg Daniluk](mailto:grzegorz.daniluk@cern.ch)
- [Alén Arias Vázquez](mailto:alen.arias.vazquez@cern.ch)
