# PCB

Hardware design for different boards related with DIOT rad-tolerant version

## Projects
- [bPOL12V](DIOT-bPOL12V/README.md)
- [System Board](system_board/README.md)

## Maintainers
- [Greg Daniluk](mailto:grzegorz.daniluk@cern.ch)
- [Alén Arias Vázquez](mailto:alen.arias.vazquez@cern.ch)
