# System Board

|![](images/system_board.jpg)|
|:--:|
| *DIOT system board V1.0* |

## Maintainers
- [Greg Daniluk](mailto:grzegorz.daniluk@cern.ch)
- [Alén Arias Vázquez](mailto:alen.arias.vazquez@cern.ch)
